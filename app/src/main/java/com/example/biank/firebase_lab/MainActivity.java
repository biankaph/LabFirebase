package com.example.biank.firebase_lab;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class MainActivity extends AppCompatActivity {
    String name1,pass1,email1;
    Button save, photo;
    EditText editTextName, editTextpass,editTextEmail;
    Uri image;
    String clave;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    // private DatabaseReference myRef;
    private static final String TAG = "Auth";
    private static final int IMAGE_CAPTURE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        //existingUser("biankaperezherrera@gmail.com", "bianka96");
        save = (Button) findViewById(R.id.save);
        photo = (Button) findViewById(R.id.photo);
        editTextName = (EditText) findViewById(R.id.editName);
        editTextEmail = (EditText) findViewById(R.id.editemail);
        editTextpass = (EditText) findViewById(R.id.editPass);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //upLoad();
                //FirebaseDatabase database = FirebaseDatabase.getInstance();
                //DatabaseReference myRef = database.getReference("User");

                name1 = editTextName.getText().toString().trim();
                email1 = editTextEmail.getText().toString().trim();
                pass1 = editTextpass.getText().toString().trim();

                newUser(email1, pass1);
                //createU();
               //String resul = buscarPersona();

            }
        });
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, IMAGE_CAPTURE);
                if (!hasCamera())
                    photo.setEnabled(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_CAPTURE) {
            image = data.getData();

        }
    }

    private boolean hasCamera() {
        return (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_ANY));
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);

    }

   /*public void existingUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }*/

    public void newUser(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            database = FirebaseDatabase.getInstance();
                            myRef = database.getReference("User");
                            Persona per = new Persona();
                            per.setPass(pass1);
                            per.setName(name1);
                            per.setEmail(email1);
                            myRef.push().setValue(per);
                            upLoad();
                            editTextName.setText("");
                            editTextEmail.setText("");
                            editTextpass.setText("");
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            validar();
                            //updateUI(null);
                        }
                        // ...
                    }
                });
    }

    public void upLoad() {
        FirebaseStorage storageRef = FirebaseStorage.getInstance();
        StorageReference storageReference =
                storageRef.getReferenceFromUrl("gs://labfire-a5605.appspot.com");


        final StorageReference photoReference = storageReference.child("photos")
                .child(image.getLastPathSegment());

        photoReference.putFile(image)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content

                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        //downloadUrl2 = downloadUrl.toString();
                        Toast.makeText(MainActivity.this, downloadUrl.toString(), Toast.LENGTH_LONG).show();
                        //downloadUrl2 = taskSnapshot.getMetadata().getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Log.d(TAG, "Fallo por :" + exception.toString());
                        Toast.makeText(MainActivity.this,
                                exception.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /*public String buscarPersona() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");
        ValueEventListener post = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Persona post = dataSnapshot.getValue(Persona.class);
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Persona per = postSnapshot.getValue(Persona.class);
                    if (per.getEmail().equals(email1)) {
                        entre = "true";
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        myRef.addValueEventListener(post);
        return entre;
    }*/

    public void validar() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Modificar Datos");

        // set dialog message
        alertDialogBuilder
                .setMessage("¿Desea modificar datos?")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        modificar();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }
    public void modificar(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("User");
        ValueEventListener post = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                    Persona user = postSnapshot.getValue(Persona.class);
                    System.out.println("email: " + user.getEmail());
                    if(user.getEmail().equals(email1)){
                        clave = postSnapshot.getKey();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("User");
                        Persona us = new Persona();
                        us.setName(name1);
                        us.setPass(pass1);
                        us.setEmail(email1);
                        us.setPhoto("hlkjlkjkl");
                       // myRef.push().setValue(per);
                        myRef.child(String.valueOf(clave)).setValue(us);
                        upLoad();
                        editTextName.setText("");
                        editTextEmail.setText("");
                        editTextpass.setText("");
                        break;
                    }
                }

                //Persona post = dataSnapshot.getValue(Persona.class);
               /* for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Persona per = postSnapshot.getValue(Persona.class);
                    if (per.getEmail().equals(email1)) {

                        postSnapshot.getRef().removeValue();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("User");
                        Persona us = new Persona();
                        us.setName(name1);
                        us.setPass(pass1);
                        us.setPhoto("hlkjlkjkl");
                        myRef.push().setValue(per);
                        //myRef.child(String.valueOf(name1)).setValue(per);
                        //upLoad();
                        editTextName.setText("");
                        editTextEmail.setText("");
                        editTextpass.setText("");
                        break;
                    }
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        myRef.addValueEventListener(post);

/*
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");
        Persona user = new Persona();
        user.setName(editTextName.getText().toString());
        user.setEmail(editTextEmail.getText().toString());
        user.setPass(editTextEmail.getText().toString());
        user.setPhoto("hlkjlkjkl");
        myRef.child(String.valueOf(name)).setValue(user);
        upLoad();
        editTextName.setText("");
        editTextEmail.setText("");
        editTextpass.setText("");*/
    }


}
