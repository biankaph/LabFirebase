package com.example.biank.firebase_lab;

import android.net.Uri;

/**
 * Created by biank on 16/5/2018.
 */

public class Persona {
    private String email = "";
    private String pass = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String photo;
    private String name = "";

}
